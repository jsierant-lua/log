local log = {};

local function func2string(call)
  local info = debug.getinfo(call);
  return string.format('%s:%s', info.short_src, info.linedefined);
end

local function tab2string(collection)
  local result = {};
  local indexes = {};
  for idx,val in ipairs(collection) do
    if type(val) == 'table' then val = tab2string(val); end
    table.insert(result, val);
    indexes[idx] = true;
  end
  for key,val in pairs(collection) do
    if not indexes[key] then
      if type(val) == 'table' then val = tab2string(val); end
      table.insert(result, string.format('%s=%s', key, val));
    end
  end
  return string.format('{%s}', table.concat(result, ', '));
end

function log.format_msg(fmt, ...)
  local args = {};
  for _,arg in ipairs({...}) do
    if type(arg) == 'table' then
      table.insert(args, tab2string(arg));
    elseif type(arg) == 'function' then
      table.insert(args, func2string(arg));
    else
      table.insert(args, arg);
    end
  end
  local success, result_or_error_message
    = pcall(string.format, fmt, unpack(args));
  if success then
    return result_or_error_message:gsub('\n', '\\n');
  end
  error(string.format('log: faild to format msg with error: %q, trace: %s'
                     ,result_or_error_message, debug.traceback()));
end

local function add_log_record(level, fmt, ...)
  local msg = log.format_msg(fmt, ...)
  local ts = log.get_timestamp()
  local loc = log.get_source_location()
  for msg_line in msg:gmatch("[^\r\n]+") do
    log.add_record(log.format_record(level, ts, loc, msg_line))
  end
end

log.levels = {
  debug = 1
 ,info = 2
 ,warning = 3
 ,error = 4
 ,none = 5
};

log.level = log.levels.debug;

log.add_record = print;

function log.get_timestamp()
  return string.format('%7.3f', 100*os.clock());
end

function log.get_source_location()
  local info = debug.getinfo(3, "Sl");
  return info.short_src .. ":" .. info.currentline;
end

function log.format_record(level, timestamp, location, msg)
  return string.format("%s|%s|%s|%s", level, timestamp, location, msg)
end

function log.debug(fmt, ...)
  if log.level > log.levels.debug then return end
  add_log_record('DBG', fmt, ...);
end

function log.info(fmt, ...)
  if log.level > log.levels.info then return end
  add_log_record('INF', fmt, ...);
end

function log.warning(fmt, ...)
  if log.level > log.levels.warning then return end
  add_log_record('WRN', fmt, ...);
end

function log.error(fmt, ...)
  if log.level > log.levels.error then return end
  add_log_record('ERR', fmt, ...);
end

return log
